 #!/usr/bin/env python3

'''
Custom dynamic inventory script for Ansible, in Python.
'''

import psycopg2
import json
from optparse import OptionParser

import requests

def get_rest(url):
  response = resquests.get(url)
  return response.json()
  
def example_inventory():
return {'_meta': {'hostvars': {}}}
   
def json_list_2():
  sql = "SELECT host,`group` FROM ansible_hosts;"  
  reg = consultar_db(sql)
  data = dict()
  groups = list(set([i[1].decode() for i in reg]))
  data["all"] = {"children": groups}
  data["_meta"] = {"hostvars": {}}
  for group in groups:
    data[group] = dict()
    data[group]["hosts"] = list()
    for x in reg:
      if x[1].decode("utf-8") == group:
        data[group]["hosts"].append(x[0].decode("utf-8"))
  return json.dumps(data,indent=3)

def json_host():
  return json.dumps({})

def main():
  parse = OptionParser()
  parse.add_option("-l", "--list", action="store_true", dest="list", default=False)
  parse.add_option("-h", "--host", action="store", dest="host", default=False)
  (option, arges) = parse.parse_args()
  if option.list:
    print(example_inventory())
  elif option.host:
    print(json_host())
  else:
     raise ValueError("Expecting either --host $HOSTNAME or --list")

if __name__ == '__main__':
  main()
